const express = require("express");
const app = express();

// require("./plugins/bodyParser")(app);
require("./plugins/cors")(app);
require("./plugins/proxy")(app);
app.use(express.static("./public"));

const routes = require("./routes")(app);

app.use(routes);

const server = require("http").createServer(app);

require("./sockets")(server);

const port = process.env.PORT || 3000;
server.listen(port, () => console.log(`Working on port ${port}`));
