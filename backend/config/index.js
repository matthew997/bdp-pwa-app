require("dotenv").config();

const env = (key, defaultValue = null) => process.env[key] || defaultValue;

const config = {
  app: {
    env: env("NODE_ENV"),
    url: env("APP_URL", "http://localhost:3000"),
    corsSites: env("APP_CORS_SITES", "http://localhost:3000"),
    port: parseInt(env("PORT", 3000)),
    host: env("APP_HOST", "127.0.0.1"),
    frontendUrl: env("APP_FRONTEND_URL"),
    jsonRequestSizeLimit: env("APP_JSON_REQUEST_SIZE_LIMIT"),
  },
  rabbitMq: {
    user: env("AMQP_USER"),
    password: env("AMQP_PASSWORD"),
    server: env("AMQP_SERVER"),
  },
  sentry: {
    dsn: env("SENTRY_DSN"),
    dsnCron: env("SENTRY_DSN_CRON"),
  },
};

module.exports = config;
