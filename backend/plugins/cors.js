const cors = require("cors");
const config = require("../config");

module.exports = (app) => {
  const corsSites = config.app.corsSites.split(",").map((site) => site.trim());
  const originsWhitelist = [config.app.frontendUrl, ...corsSites];

  const corsOptions = {
    origin(origin, callback) {
      callback(null, true);
      // if (originsWhitelist.includes(origin) || !origin) {
      //   callback(null, true);
      // } else {
      //   callback(new Error("Not allowed by CORS"));
      // }
    },
    credentials: true,
  };

  app.use(cors(corsOptions));
};
