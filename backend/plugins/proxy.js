const { createProxyMiddleware } = require("http-proxy-middleware");
const config = require("../config");

module.exports = (app) => {
  const options = {
    target: "https://api.flotea.pl",
    changeOrigin: true,
    ws: false,
    pathRewrite: {
      "^/api": "/",
    },
  };

  const payuProxy = {
    target: "https://secure.payu.com",
    changeOrigin: true,
    ws: false,
    pathRewrite: {
      "^/pay": "/",
    },
  };

  if (config.app.env === "development") {
    const proxy = createProxyMiddleware(options);
    const proxyPayU = createProxyMiddleware(payuProxy);

    app.use("/api", proxy);
    app.use("/pay", proxyPayU);
  }
};
