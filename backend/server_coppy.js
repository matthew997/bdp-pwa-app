// var express = require("express");
// var Buffer = require("buffer").Buffer;
// var rabbitConn = require("./connection");
// var app = express();
// require("./cors")(app);
// require("./bodyParser")(app);
// var router = express.Router();
// var server = require("http").Server(app);
// var io = require("socket.io")(server, {
//   allowEIO3: true,
//   cors: {
//     origin: "*",
//     methods: ["GET", "POST"],
//     allowedHeaders: ["my-custom-header"],
//     credentials: true,
//   },
// });

// const r = new rabbitConn();

// app.use(express.static("public"));

// io.on("connection", async () => {
//   console.log("Client connected...");

//   const conn = await r.conn();
//   const ch = await conn.createChannel();
//   var ex = "tasks";

//   ch.assertExchange(ex, "fanout", {
//     durable: false,
//   });

//   ch.assertQueue(ex, { durable: false });
//   ch.bindQueue(ex, ex, "");
//   ch.get(ex, (err, ok) => {
//     console.log(ok);
//   });

//   ch.consume(ex, (msg) => {
//     console.log(" [x] Received %s", msg.content.toString());
//     io.emit("messages", msg.content.toString());
//   });
// });

// io.on("disconnecting", (reason) => {
//   console.log("Got disconnect!");
//   socket.emit("disconnected");
// });
// //------------------------------------------------------------------

// app.use(express.static("public"));
// app.use("/api", router);

// router.route("/chat").post(async (req, res) => {
//   const ch = await r.mainChannel;

//   var ex = "tasks";
//   const msg = JSON.stringify(req.body);

//   ch.assertExchange(ex, "fanout", {
//     durable: false,
//   });
//   ch.publish(ex, "", Buffer.from(msg));
//   console.log(" [x] Sent %s", msg);
//   // ch.assertQueue(q);
//   // ch.sendToQueue(q, Buffer.from(msg), { persistent: true });
// });

// server.listen(3030, "0.0.0.0", function() {
//   console.log("Chat at localhost:3030");
// });

const amqp = require("amqplib/callback_api");
const axios = require("axios");
const express = require("express");
const app = express();
var soc;
var cons = new Array();
var ch = null;

var server = require("http").Server(app);
var io = require("socket.io")(server, {
  allowEIO3: true,
  cors: {
    origin: "*",
    methods: ["GET", "POST"],
    allowedHeaders: ["my-custom-header"],
    credentials: true,
  },
});

amqp.connect(
  "amqp://xittmwea:NJhPgV1dun4f02zRaz4yQRYQsddIikw_@bear.rmq.cloudamqp.com/xittmwea",
  function(err, conn) {
    if (err) console.error(err);

    conn.on("error", function(err) {
      if (err.message !== "Connection closing") {
        console.error("[AMQP] conn error", err.message);
      }
    });
    conn.on("close", function() {
      console.error("[AMQP] reconnecting");
    });

    conn.createChannel(function(err, channel) {
      ch = channel;

      io.sockets.on("connection", function(socket) {
        console.log("new client");

        socket.on("for_server", function(data) {
          console.log(data);
          if (typeof data != "undefined" && typeof data.data != "undefined") {
            const msg = data.data[0];
            if (ch) {
              console.log("sended " + data.request_uuid + " " + data.uuid);
              ch.publish(
                "passengers",
                data.uuid,
                Buffer.from(JSON.stringify(msg))
              );
            } else console.log("no channel for send");
          }

          axios
            .get(
              "https://xittmwea:NJhPgV1dun4f02zRaz4yQRYQsddIikw_@bear.rmq.cloudamqp.com/api/queues/xittmwea/" +
                data.request_uuid
            )
            .then((response) => {
              if (response.statusText == "OK") {
                if (ch && typeof cons[data.request_uuid] === "undefined") {
                  ch.consume(data.request_uuid, function(msg) {
                    cons[data.request_uuid] = new Date().getTime();
                    console.log(
                      "recive " + data.request_uuid + " " + data.uuid
                    );
                    io.sockets.emit("for_client", {
                      msg: msg,
                      content: msg.content.toString(),
                    });
                  });
                } else if (!ch) {
                  console.log("no channel for receive");
                }
              }
            })
            .catch((error) => {
              console.log("error api");
            });
        });
      });
    });
  }
);

server.listen(3030, "0.0.0.0", function() {
  console.log("Chat at localhost:3030");
});
