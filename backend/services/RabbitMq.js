const amqp = require("amqplib");
const { rabbitMq } = require("../config");
const amqpURL = `amqp://${rabbitMq.user}:${rabbitMq.password}@${rabbitMq.server}/${rabbitMq.user}`;

class RabbitMq {
  constructor() {
    if (!RabbitMq.instance) {
      this.open = this.connect();
      RabbitMq.instance = this;
    }

    return RabbitMq.instance;
  }

  async connect() {
    return await amqp.connect(amqpURL);
  }

  async conn() {
    return await this.open;
  }
}

const rabbit = new RabbitMq();

module.exports = { rabbit };
