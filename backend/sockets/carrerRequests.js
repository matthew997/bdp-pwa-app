const { rabbit } = require("../services/RabbitMq");
const axios = require("axios");
const { rabbitMq } = require("../config");
const amqpApiUrl = `https://${rabbitMq.user}:${rabbitMq.password}@${rabbitMq.server}/api/queues/${rabbitMq.user}/`;

module.exports = async (io) => {
  const conn = await rabbit.conn();
  const channel = await conn.createChannel();

  io.sockets.on("connection", (socket) => {
    socket.on("for_server_carrer_requests", async (data) => {
      const { request_uuid } = data;
      socket.join(request_uuid);

      const response = await axios.get(amqpApiUrl + request_uuid);

      if (response.statusText == "OK") {
        await channel.consume(request_uuid, (msg) => {
          const contentString = msg.content.toString();
          const content = JSON.parse(contentString);
          content.uuid = msg.properties.correlationId;

          socket.emit("for_client_carrer_requests", {
            content,
            request_uuid,
          });
        });
      }
    });
  });
};
