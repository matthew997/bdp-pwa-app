const fs = require("fs");

var options = {
  allowEIO3: true,
  cors: {
    origin: "*",
    methods: ["GET", "POST"],
    allowedHeaders: ["room"],
    credentials: true,
  },
};

module.exports = (server) => {
  const io = require("socket.io")(server, options);

  fs.readdirSync(__dirname).forEach((route) => {
    route = route.split(".")[0];

    if (route === "index") {
      return;
    }

    require(`./${route}`)(io);
  });
};
