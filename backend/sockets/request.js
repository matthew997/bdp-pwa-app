const { rabbit } = require("../services/RabbitMq");
const Buffer = require("buffer").Buffer;
const axios = require("axios");
const { rabbitMq } = require("../config");
const amqpApiUrl = `https://${rabbitMq.user}:${rabbitMq.password}@${rabbitMq.server}/api/queues/${rabbitMq.user}/`;

module.exports = async (io) => {
  const conn = await rabbit.conn();
  const channel = await conn.createChannel();

  io.sockets.on("connection", (socket) => {
    socket.on("for_server", async (data) => {
      const { data: message, request_uuid, uuid } = data;

      socket.join(uuid);

      if (typeof data != "undefined" && typeof message != "undefined") {
        const msg = message.shift();

        channel.publish(
          "passengers",
          data.uuid,
          Buffer.from(JSON.stringify(msg))
        );
      }

      const response = await axios.get(amqpApiUrl + request_uuid);

      if (response.statusText == "OK") {
        channel.consume(request_uuid, (msg) => {
          socket.to(uuid).emit("for_client", {
            msg,
            content: msg.content.toString(),
          });
        });
      }
    });
  });
};

// to("room1")
