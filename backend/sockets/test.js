const { rabbit } = require("../services/RabbitMq");
const Buffer = require("buffer").Buffer;
const axios = require("axios");
const { rabbitMq } = require("../config");
const amqpApiUrl = `https://${rabbitMq.user}:${rabbitMq.password}@${rabbitMq.server}/api/queues/${rabbitMq.user}/`;

module.exports = async (io) => {
  const conn = await rabbit.conn();
  const channel = await conn.createChannel();

  io.sockets.on("connection", (socket) => {
    socket.on("for_server_test", async (data) => {
      const { data: message } = data;

      if (typeof data != "undefined" && typeof message != "undefined") {
        const msg = message.shift();

        channel.publish(
          "passengers",
          data.uuid,
          Buffer.from(JSON.stringify(msg))
        );
      }

      const { request_uuid } = data;
      const response = await axios.get(amqpApiUrl + request_uuid);

      // if (response.statusText == "OK") {
      await channel.consume(request_uuid, (msg) => {
        socket.emit("for_client_test", {
          msg,
          content: msg.content.toString(),
        });
      });
      // }
    });
  });
};
