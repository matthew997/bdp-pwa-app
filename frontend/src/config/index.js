require('dotenv');

module.exports = {
    apiBaseUrl: process.env.VUE_APP_API_BASE_URL,
    frontendUrl: process.env.VUE_APP_FRONTEND_URL,
    socketUrl: process.env.VUE_APP_SOCKET_URL,
    mediaBaseUrl: process.env.VUE_APP_MEDIA_BASE_URL,
    publicPath: '/'
};
