import Vue from 'vue';
import App from './app';
import router from '@router';
import store from '@state/store';
import Vuelidate from 'vuelidate';
import VueMaterial from 'vue-material';
import VueToast from 'vue-toast-notification';

import 'vue-swipe-actions/dist/vue-swipe-actions.css';
import 'vue-material/dist/vue-material.min.css';
import 'vue-material/dist/theme/default.css';
import 'vue-toast-notification/dist/theme-sugar.css';
import '@assets/styles/main.scss';

import '../bootstrap/axios.config';
import './registerServiceWorker';

Vue.config.productionTip = process.env.NODE_ENV === 'production';

Vue.use(Vuelidate);
Vue.use(VueMaterial);
Vue.use(VueToast);

if ('serviceWorker' in navigator) {
    navigator.serviceWorker
        .register('./src/sw.js', { scope: './src/' })
        .then(reg => {
            // registration worked
            console.log('Registration succeeded. Scope is ' + reg.scope);
        })
        .catch(error => {
            // registration failed
            console.log('Registration failed with ' + error);
        });
}

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app');
