import store from '@state/store';

export default [
    {
        path: '/login',
        name: 'login',
        component: () => lazyLoadView(import('@views/auth/Login')),
        meta: {
            layout: 'auth',
            beforeResolve(routeTo, routeFrom, next) {
                if (store.getters['auth/loggedIn']) {
                    next({ name: 'home' });
                } else {
                    next();
                }
            }
        }
    },
    {
        path: '/logout',
        name: 'logout',
        meta: {
            authRequired: true,
            beforeResolve(routeTo, routeFrom, next) {
                store.dispatch('auth/logout');

                next({ path: '/' });
            }
        }
    },
    {
        path: '/reset-password',
        name: 'reset-password',
        component: () => lazyLoadView(import('@views/auth/ResetPassword')),
        props: route => ({ token: route.params.token }),
        meta: {
            layout: 'auth',
            beforeResolve(routeTo, routeFrom, next) {
                if (store.getters['auth/loggedIn']) {
                    next({ name: 'home' });
                } else {
                    next();
                }
            }
        }
    },

    {
        path: '/:some_text/:inquiryUUID',
        name: 'route',
        component: () => lazyLoadView(import('@views/Route')),
        meta: {
            layout: 'default',
            authRequired: true
        }
    },

    {
        path: '/',
        name: 'home',
        component: () => lazyLoadView(import('@views/Home')),
        meta: {
            layout: 'default',
            authRequired: true
        }
    },

    {
        path: '/taken-passengers',
        name: 'taken-passengers',
        component: () => lazyLoadView(import('@views/TakenPassengers')),
        meta: {
            layout: 'default',
            authRequired: true
        }
    },

    {
        path: '/settings',
        name: 'settings',
        component: () => lazyLoadView(import('@views/Settings')),
        meta: {
            layout: 'basicMain',
            authRequired: true
        }
    },

    {
        path: '/deposits',
        name: 'deposits',
        component: () => lazyLoadView(import('@views/Deposits')),
        meta: {
            layout: 'default',
            authRequired: true
        }
    },

    {
        path: '/*',
        name: 'error',
        component: () => lazyLoadView(import('@views/errors/NotFound')),
        meta: {
            layout: 'error',
            authRequired: false
        }
    }
];

function lazyLoadView(AsyncView) {
    const AsyncHandler = () => ({
        component: AsyncView,
        delay: 400,
        timeout: 10000
    });

    return Promise.resolve({
        functional: true,
        render(h, { data, children }) {
            return h(AsyncHandler, data, children);
        }
    });
}
