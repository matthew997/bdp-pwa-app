import axios from 'axios';

export const state = {
    currentUser: getSavedState('user'),
    refreshToken: getSavedState('refreshToken'),
    token: getSavedState('token'),
    connectionError: false
};

export const getters = {
    loggedIn(state) {
        return !!state.currentUser;
    },

    currentUser: state => state.currentUser,

    connectionError: state => state.connectionError
};

export const mutations = {
    SET_TOKENS(state, data) {
        saveState('refreshToken', data.refresh_token);
        saveState('token', data.access_token);
    },

    SET_USER(state, data) {
        const user = {
            account_type: data[0].account_type,
            company_name: data[0].company_name,
            avatar: data[0].avatar,
            email: data[0].email,
            carrierUUID: data[0].uuid,
            credit: data[0].credit,
            medal: data[0].medal,
            next_medal: data[0].next_medal,
            payment_type: data[0].payment_type,
            default_call_price: data[0].default_call_price,
            default_take_price: data[0].default_take_price
        };

        saveState('user', user);
        state.currentUser = user;
    },

    LOGOUT(state, newValue) {
        localStorage.user = null;
        localStorage.token = null;
        localStorage.refreshToken = null;
    },

    CONNECTION_ERROR(state) {
        this.state.connectionError = true;
    }
};

export const actions = {
    init({ state, dispatch }) {
        dispatch('validate');
    },

    async logout({ commit }) {
        commit('LOGOUT', null);
    },

    async login({ commit, dispatch, getters }, params) {
        if (getters.loggedIn) {
            return dispatch('validate');
        }

        const body = {
            grant_type: 'password',
            scope: 'user',
            username: params.email,
            password: params.password,
            client_id: 'testclient',
            client_secret: 'testpass'
        };
        const headers = {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Accept-Language': 'pl-PL'
        };

        try {
            const { data } = await axios.post('oauth2/token', body, {
                headers
            });

            commit('SET_TOKENS', data);

            return dispatch('validate');
        } catch (error) {
            throw error;
        }
    },

    async refreshToken({ commit }) {
        const body = {
            grant_type: 'refresh_token',
            refresh_token: JSON.parse(localStorage.refreshToken),
            client_id: 'testclient',
            client_secret: 'testpass',
            scope: 'user'
        };

        try {
            const { data } = await axios.post('oauth2/token', body);

            commit('SET_TOKENS', data);
        } catch (error) {
            if (error.response && error.response.status <= 400) {
                commit('LOGOUT');
            }
        }
    },

    async validate({ commit, dispatch }) {
        if (localStorage.token) {
            axios.defaults.headers.common['Authorization'] =
                'Bearer ' + JSON.parse(localStorage.token);
        }

        try {
            const { data } = await axios.get('me');

            if (data) {
                commit('SET_USER', data);
            }

            return data;
        } catch (error) {
            if (error.response && error.response.status === 401) {
                commit('LOGOUT');
            }
        }
    },

    async resetPassword({ commit, state }, formData) {
        const headers = {
            'Content-Type': 'multipart/form-data'
        };

        await axios.post(`new-password`, formData, { headers });
    }
};

function getSavedState(key) {
    return JSON.parse(window.localStorage.getItem(key));
}

function saveState(key, state) {
    window.localStorage.setItem(key, JSON.stringify(state));
}
