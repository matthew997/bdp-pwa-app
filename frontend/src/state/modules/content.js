import axios from 'axios';

export const state = {
    routeDetails: {},
    carrierDetails: {},
    views: 0,
    deposit: {},
    carrierDeposits: []
};

export const getters = {
    routeDetails: state => state.routeDetails,
    carrierDetails: state => state.carrierDetails,
    views: state => state.views,
    deposit: state => state.deposit,
    carrierDeposits: state => state.carrierDeposits
};

export const mutations = {
    ADD_ROUTE_DETAILS(state, response) {
        state.routeDetails = response;
    },

    UPDATE_ROUTE_DETAILS(state, data) {
        state.routeDetails = { ...state.routeDetails, ...data };
    },

    ADD_PHONE_AND_MORE_DETAILS(state, data) {
        state.routeDetails = data;
    },

    ADD_CARRIER_DETAILS(state, data) {
        state.carrierDetails = data;
    },

    ADD_VIEWS(state, data) {
        state.views = data;
    },

    ADD_DEPOSIT(state, data) {
        state.deposit = data;
    },

    ADD_CARRIER_DEPOSITS(state, data) {
        state.carrierDeposits = data.deposit;
    }
};

export const actions = {
    async getRoute({ commit }, inquiryUUID) {
        const { data } = await axios.get(`get-app-route/${inquiryUUID}`);

        commit('ADD_ROUTE_DETAILS', data);

        return data;
    },

    async getPhone({ commit }, inquiryUUID) {
        const { data } = await axios.get(`get-request-phone/${inquiryUUID}`);

        commit('ADD_PHONE_AND_MORE_DETAILS', data);

        return data;
    },

    async getCalled({ commit }, params) {
        const { carrierUUID, inquiryUUID } = params;

        const { data } = await axios.get(
            `get-called/${carrierUUID}/${inquiryUUID}`
        );

        return data;
    },

    async getAnyCall({ commit }, inquiryUUID) {
        const { data } = await axios.get(`any-call/${inquiryUUID}`);

        return data;
    },

    async getCarrier({ commit }, carrierUUID) {
        const { data } = await axios.get(`get-carrier/${carrierUUID}`);

        commit('ADD_CARRIER_DETAILS', data);

        return data;
    },

    async returnCarrier({ commit }, carrierUUID) {
        const { data } = await axios.get(`get-carrier/${carrierUUID}`);

        return data;
    },

    async getViews({ commit }, params) {
        const { carrierUUID, inquiryUUID } = params;

        const { data } = await axios.get(
            `get-viewed/${carrierUUID}/${inquiryUUID}`
        );

        commit('ADD_VIEWS', data);

        return data;
    },

    async getDepositApp({ commit }, params) {
        const { carrierUUID, inquiryUUID } = params;

        const { data } = await axios.get(
            `get-deposit-app/${carrierUUID}/${inquiryUUID}`
        );

        commit('ADD_DEPOSIT', data);

        return data;
    },

    async getAnyDepo({ commit }, inquiryUUID) {
        const { data } = await axios.get(`any-depo/${inquiryUUID}`);

        return data;
    },

    async getDeposit({ commit }) {
        const { data } = await axios.get(`get-deposit`);

        commit('ADD_CARRIER_DEPOSITS', data);

        return data;
    },

    async sendDeposit({ commit }, params) {
        const { data } = await axios.post(`send-deposit`, params, {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        });

        return data;
    }
};
