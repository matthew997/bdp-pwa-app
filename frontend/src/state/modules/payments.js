import axios from 'axios';

export const state = {};

export const getters = {};

export const mutations = {};

export const actions = {
    async buyPoints({ commit }, params) {
        return await axios.post(`/buy-credit`, params);
    },

    async buyMedal({ commit }, params) {
        return await axios.post('/buy-medal', params);
    }
};
