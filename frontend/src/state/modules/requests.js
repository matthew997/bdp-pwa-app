import axios from 'axios';

export const state = {
    lastCarrierRequests: []
};

export const getters = {
    carrierRequests: state => state.lastCarrierRequests
};

export const mutations = {
    ADD_LAST_REQUESTS(state, response) {
        state.lastCarrierRequests = response;
    },

    ADD_REQUEST(state, data) {
        const coppy = state.lastCarrierRequests;
        coppy.unshift(data.content);

        const uniqueArray = coppy.filter(({ uuid }, index) => {
            return (
                index ===
                coppy.findIndex(obj => {
                    return obj.uuid === uuid;
                })
            );
        });

        state.lastCarrierRequests = uniqueArray;
    }
};

export const actions = {
    async lastCarrierRequests({ commit }) {
        const { data } = await axios.post(`last-carrier-requests/24`);

        commit('ADD_LAST_REQUESTS', data);

        return data;
    }
};
