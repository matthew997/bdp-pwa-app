import axios from 'axios';

export const state = {
    websiteOpen: false
};

export const getters = {
    websiteOpen: state => state.websiteOpen,
    noticonf: state => state.noticonf
};

export const mutations = {
    WEBSITE_OPEN(state, data) {
        state.websiteOpen = data;
    }
};

export const actions = {
    async getNoticonf({ commit }) {
        const { data } = await axios.post(`noticonf`);

        return data;
    },

    async changeNoticonf({ commit }, params) {
        const { data } = await axios.post(`noticonf`, params, {
            headers: {
                'Content-Type': 'application/json'
            }
        });

        return data;
    }
};
