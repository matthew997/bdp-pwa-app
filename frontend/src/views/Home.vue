<template>
    <div
        class="md-layout-item md-large-size-100 md-medium-size-100 md-small-size-100 md-xsmall-size-100"
    >
        <swipe-list
            v-if="carrierRequests.length"
            ref="list"
            class="card"
            :items="carrierRequests"
            transition-key="id"
            @swipeout:click="itemClick"
        >
            <template slot-scope="{ item }">
                <div>
                    <div
                        v-if="checkIfPartner(item.partner)"
                        :class="item.taken_by ? 'message-gray' : 'message-blue'"
                    >
                        Zapytanie PARTNERA ? zł
                    </div>
                    <div
                        v-else
                        :class="
                            item.taken_by ? 'message-gray' : 'message-green'
                        "
                    >
                        Zapytanie PASAŻERA ? zł
                    </div>
                    <div class="time-ago">{{ timeAgo(item.created_at) }}</div>
                </div>
                <div class="data-time">{{ formatDate(item.requested_at) }}</div>
                <div>
                    <div class="icon">
                        <img
                            v-if="item.taken_by"
                            src="@assets/images/icon-route-gray.svg"
                        />
                        <img v-else src="@assets/images/icon-route.svg" />
                    </div>
                    <div class="route">
                        <div>{{ item.from }}</div>
                        <div>{{ item.to }}</div>
                    </div>
                    <div class="icon-phone">
                        <img
                            v-if="item.taken_by"
                            src="@assets/images/icon-phone.png"
                        />
                        <img v-else src="@assets/images/icon_phone_green.png" />
                    </div>
                </div>
                <div>
                    <div v-for="(value, index) in item.attributes" :key="index">
                        <img
                            v-if="value"
                            :src="
                                require(`@assets/images/${attributes[index].icon}`)
                            "
                        />
                    </div>
                </div>
            </template>

            <template slot="left" slot-scope="{ item }">
                <div class="swipeout-action action-panel-left">
                    <div @click="itemClick(item)">
                        <img src="@assets/images/icon-add.png" />
                        Otwórz zapytanie
                    </div>
                    <div
                        v-if="
                            item.taken_by === currentUser.carrierUUID ||
                                !item.taken_by
                        "
                        @click="checkCreditAndCall(item)"
                    >
                        <img
                            v-if="!loading"
                            src="@assets/images/icon-call.png"
                        />
                        <img v-else src="@assets/images/loader.gif" />
                        Zadzwoń
                    </div>
                    <div v-else @click="hasBeenTaken(item)">
                        <img
                            v-if="!loading"
                            src="@assets/images/icon-call.png"
                        />
                        <img v-else src="@assets/images/loader.gif" />
                        Zadzwoń
                    </div>
                </div>
            </template>
        </swipe-list>
        <md-card v-else>
            <div class="md-body-1">
                Nie ma zapytań prosimy o cierpliwość. Upewnij się, że dobrze
                skonfigurowałeś obszar obsługi pasażerów, zmodyfikuj go jeśli
                otrzymujesz zbyt mało zapytań.
            </div>
        </md-card>
        <snackbar :show="isTakenByOther" :text="passengerHasBeenTakenBy" />
        <snackbar :show="showCalling" :text="passengerIsTalkingMessage" />
        <too-little-credit-modal
            :show="tooLittleCredit"
            :carrierUUID="currentUser.carrierUUID"
            @closeTooLittleCredit="() => (tooLittleCredit = false)"
        />
    </div>
</template>
<script>
import { mapGetters, mapActions, mapMutations } from 'vuex';
import { SwipeList, SwipeOut } from 'vue-swipe-actions';
import Snackbar from '@components/elements/Snackbar';
import TooLittleCreditModal from '@components/modals/tooLittleCreditModal';
import moment from 'moment';
import config from '@config';
const io = require('socket.io-client');
const socket = io(`${config.socketUrl}`);

export default {
    components: {
        SwipeOut,
        SwipeList,
        Snackbar,
        TooLittleCreditModal
    },

    data() {
        return {
            loading: false,
            tooLittleCredit: false,
            showCalling: false,
            isTakenByOther: false,
            takenByCarrier: '',
            price: { call: 5, take: 55 },
            attributes: [
                {
                    message: 'Pasażer chce wysłać lub zabrać ze sobą paczkę',
                    icon: 'icon_box.png'
                },
                {
                    message: 'Pasażer chce zabrać zwierzęta',
                    icon: 'icon_pet.png'
                },
                {
                    message: 'Pasażer chce zabrać wózek',
                    icon: 'icon_wheelchair.png'
                },
                {
                    message:
                        'Pasazer może wyjechać w inny dzień niż wskazany na kalendarzu',
                    icon: 'icon-calendar-plus.png'
                },
                {
                    message: 'Pasażer preferuje wyjazd wieczorem',
                    icon: 'icon-moon.png'
                },
                {
                    message: 'Pasażer preferuje wyjazd rano',
                    icon: 'icon-sun.png'
                },
                {
                    message: 'Pasażer planuje podróż powrotną',
                    icon: 'icon-return.png'
                }
            ]
        };
    },

    computed: {
        ...mapGetters({
            currentUser: 'auth/currentUser',
            carrierRequests: 'requests/carrierRequests',
            websiteIsOpen: 'settings/websiteOpen'
        }),

        passengerIsTalkingMessage() {
            return 'Pasażer rozmawia z innym przewoźnikiem. Zaczekaj i zadzwoń za kilka minut.';
        },

        passengerHasBeenTakenBy() {
            return ` Spóźniłeś się! Pasażer został zabrany przez przewoźnika: ${this.takenByCarrier}`;
        }
    },

    methods: {
        ...mapMutations({
            addRequest: 'requests/ADD_REQUEST',
            websiteOpen: 'settings/WEBSITE_OPEN'
        }),

        ...mapActions({
            getPhone: 'content/getPhone',
            getCalled: 'content/getCalled',
            getAnyCall: 'content/getAnyCall',
            getCarrier: 'auth/validate',
            returnCarrier: 'content/returnCarrier',
            lastCarrierRequests: 'requests/lastCarrierRequests'
        }),

        timeAgo(dataTime) {
            return moment(dataTime)
                .locale('pl')
                .fromNow();
        },

        formatDate(dataTime) {
            return moment(dataTime)
                .locale('pl')
                .format('DD MMM YYYY');
        },

        checkIfPartner(partner) {
            if (partner === '00000000-0000-0000-0000-000000000000' || null) {
                return false;
            } else {
                return true;
            }
        },

        async connectToSocket() {
            socket.emit('for_server_carrer_requests', {
                request_uuid: this.currentUser.carrierUUID
            });

            socket.on('for_client_carrer_requests', async data => {
                const { request_uuid } = data;
                if (request_uuid == this.currentUser.carrierUUID) {
                    this.addRequest(data);
                }
            });
        },

        itemClick(e) {
            this.$router.push(`/app/${e.uuid}`);
        },

        checkCreditAndCall(e) {
            if (this.currentUser.credit < this.price.call) {
                this.tooLittleCredit = true;
            } else {
                this.callToPassenger(e);
            }
        },

        async callToPassenger(e) {
            this.loading = true;
            const msg = [{ cmd: 'call' }];
            const anyCall = await this.getAnyCall(e.uuid);
            const { carrierUUID } = this.currentUser;
            const { call: iCall } = await this.getCalled({
                carrierUUID,
                inquiryUUID: e.uuid
            });

            if (iCall) {
                const response = await this.getPhone(e.uuid);
                if (response) {
                    this.loading = false;
                    location.href = 'tel:' + response.phone;
                }
            } else if (!!anyCall && anyCall.carrier !== carrierUUID) {
                this.showCalling = true;
                this.loading = false;
            } else {
                this.loading = false;
                socket.emit('for_server', {
                    data: msg,
                    request_uuid: e.uuid,
                    uuid: carrierUUID + '.' + e.uuid
                });
                const response = await this.getPhone(e.uuid);
                if (response) {
                    this.loading = false;
                    location.href = 'tel:' + response.phone;
                }
            }
        },

        checkCredit() {
            if (this.currentUser.credit < this.price.take) {
                this.tooLittleCredit = true;
            }
        },

        async hasBeenTaken(item) {
            this.loading = true;
            const { company_name } = await this.returnCarrier(item.taken_by);

            this.takenByCarrier = company_name;
            this.loading = false;
            this.isTakenByOther = true;

            setTimeout(() => {
                this.isTakenByOther = false;
            }, 3000);
        }
    },

    async mounted() {
        this.getCarrier();
        await this.lastCarrierRequests();
        this.connectToSocket();

        if (!this.websiteIsOpen) {
            this.checkCredit();
            this.websiteOpen(true);
        }
    }
};
</script>
