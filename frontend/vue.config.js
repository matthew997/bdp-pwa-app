const appConfig = require('./src/app.config');
const config = require('./src/config');
const HtmlWebpackBannerPlugin = require('html-webpack-banner-plugin');
const logoComment = ``;

module.exports = {
    lintOnSave: false,
    publicPath: config.publicPath,

    pwa: {
        name: 'Bus Do Polski',
        themeColor: '#181863',
        msTileColor: '#181863',
        appleMobileWebAppCapable: 'yes',
        appleMobileWebAppStatusBarStyle: 'Black-translucent',
        start_url: '/index.html',
        display: 'standalone',
        orientation: 'portrait',
        manifestOptions: {
            background_color: '#F2F2F2'
        },

        // configure the workbox plugin
        workboxPluginMode: 'GenerateSW',
        workboxOptions: {}
    },

    configureWebpack: {
        // We provide the app's title in Webpack's name field, so that
        // it can be accessed in index.html to inject the correct title.
        name: appConfig.title,
        // Set up all the aliases we use in our app.
        resolve: {
            alias: require('./aliases.config').webpack
        },
        performance: {
            // Only enable performance hints for production builds,
            // outside of tests.
            hints:
                process.env.NODE_ENV === 'production' &&
                !process.env.VUE_APP_TEST &&
                'warning'
        },
        plugins: [
            new HtmlWebpackBannerPlugin({
                banner: logoComment
            })
        ]
    },
    css: {
        sourceMap: true
    }
};
